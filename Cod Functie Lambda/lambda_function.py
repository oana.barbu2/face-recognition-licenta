import boto3

# Biblioteca `boto3` este importata pentru a interactiona cu serviciile AWS, in acest caz, Amazon SQS.

# Credentialele AWS si configurarea
access_key = "AKIAZBUC6RSJ7ZP34JY4" # Cheia de acces AWS (de la user-ul IAM)
access_secret = "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" # Cheia secreta de acces AWS (de la user-ul IAM)
region ="us-east-1" # Regiunea aleasa pentru servicii
queue_url = "https://sqs.us-east-1.amazonaws.com/621971147923/RPIQueue" # URL-ul pentru coada SQS

# Crearea unui client SQS folosind credentialele AWS si configuratia
client = boto3.client('sqs', aws_access_key_id=access_key, aws_secret_access_key=access_secret, region_name=region)

# Construirea unui obiect de tip raspuns vorbit cu parametrii dati
def build_speechlet_response(title, output, reprompt_text, should_end_session):
    # Functia construieste un raspuns pentru skill-ul Alexa
    # Parametrii sunt titlul, textul de iesire, textul reprompt si un flag care indica daca sesiunea ar trebui sa se termine

    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }

# Construirea unui obiect raspuns cu atributele sesiunii date si raspunsul generat de functia de mai sus
def build_response(session_attributes, speechlet_response):
    # Functia construieste raspunsul complet pentru Alexa skills.
    # Parametrii sunt atributele sesiunii si raspunsul generat de functia de mai sus

    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }

# Trimiterea unui mesaj catre coada SQS specificata
def post_message(client, message_body, url):
    # Functia trimite un mesaj unei cozi SQS
    # Parametrii sunt clientul SQS, corpul mesajului si URl-ul cozii

    response = client.send_message(QueueUrl=url, MessageBody=message_body)

# Functia principala lambda_handler
def lambda_handler(event, context):
    # Functia se executa atunci cand functia Lambda este invocata
    # Parametrii sunt datele evnimentului si contextul de executie

    client = boto3.client('sqs', aws_access_key_id=access_key, aws_secret_access_key=access_secret, region_name=region)
    
    # Preluarea numelui intentiei din Alexa request event (evenimentul de cerere)
    intent_name = event['request']['intent']['name']

    if intent_name == "LaunchSkill":
        # Daca intentia este "LaunchSkill", se trimite un mesaj cu corpul "face_recognition" catre coada specificata
        post_message(client, 'face_recognition', queue_url)
        # Raspunsul imediat livrat de Alexa pentru a anunta utilizatorul care a fost intentia recunoscuta
        # Aici: Recunoasterea faciala se incarca
        message = "Face recognition is loading"
    elif intent_name == "AddPerson":
        # Daca intentia este "AddPerson", se trimite un mesaj cu corpul "add_person" catre coada specificata
        post_message(client, 'add_person', queue_url)
        # Raspunsul imediat livrat de Alexa pentru a anunta utilizatorul care a fost intentia recunoscuta
        #Aici: Se poate apasa tasta Space pentru a face o poza sau tasta Escape pentru a inchide
        message = "You can press Space to take a photo or Escape to close"
    elif intent_name == "AMAZON.HelpIntent":
        # Raspunsul imediat livrat de Alexa pentru a anunta utilizatorul care a fost intentia recunoscuta
        # Aici este mesajul de ajutor care iti da exemple de cum poti actiona celelalte intentii
        message = "You can either start face recognition by saying Alexa ask face recognition to start or you can add a person in the contacts by saying Alexa ask face recognition to add person"
    else:
        # Pentru orice alta intentie care nu se incadreaza in aceste cazuri, se anunta utilizatorul ca nu s-a inteles cererea
        message = "Sorry, but I do not understand that request"

    # Construirea raspunsului de timp discurs vocal si returneaza obiectul raspuns
    speechlet = build_speechlet_response("Mirror Status", message, "", "true")
    return build_response({}, speechlet)
