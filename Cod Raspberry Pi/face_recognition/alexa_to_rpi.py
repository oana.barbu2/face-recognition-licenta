# Importarea bibliotecilor necesare
import boto3  # AWS SDK pentru Python
import os  # Interfata sistemului de operare
import time  # Functii legate de timp


# Credentialele AWS si configurarea
access_key = "AKIAZBUC6RSJ7ZP34JY4"  # Cheia de acces AWS (de la user-ul IAM)
access_secret = "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"  # Cheia secreta de acces AWS (de la user-ul IAM)
region = "us-east-1"  # Regiunea aleasa pentru servicii
queue_url = "https://sqs.us-east-1.amazonaws.com/621971147923/RPIQueue"  # URL-ul pentru coada SQS

def pop_message(client, url):
    # Primirea de mesaje din coada SQS
    response = client.receive_message(QueueUrl=url, MaxNumberOfMessages=10)

    # Obtinerea corpului (body) si a receipt handle ale primului mesaj
    message = response['Messages'][0]['Body']
    receipt = response['Messages'][0]['ReceiptHandle']

    # Stergerea mesajului primit din coada SQS
    client.delete_message(QueueUrl=url, ReceiptHandle=receipt)

    # Returnarea continutului mesajului
    return message

# Crearea unui client SQS folosind credentialele AWS si configuratia
client = boto3.client('sqs', aws_access_key_id=access_key, aws_secret_access_key=access_secret, region_name=region)

time_waiting = 20

# Setarea timpului de asteptare pentru primirea mesajelor din coada SQS
client.set_queue_attributes(QueueUrl=queue_url, Attributes={'ReceiveMessageWaitTimeSeconds': str(time_waiting)})

start_time = time.time()

# Verificarea continua a mesajelor pentru 60 de secunde
while (time.time() - start_time < 60):
    print("Verificare...")
    try:
        # Pop-uirea (scoaterea) mesajului din coada SQS
        message = pop_message(client, queue_url)
        print(message)

        # Daca mesajul primit este "face_recognition", se executa scriptul de recunoastere faciala, "rpi_face_recognition.py"
        if message == "face_recognition":
            os.system("python /home/licenta1/face_recognition/rpi_face_recognition.py")
        # Daca mesajul primit este "add_person", se executa scriptul pentru contacte, "rpi_contacts.py"
        elif message == "add_person":
            os.system("python /home/licenta1/face_recognition/rpi_contacts.py")

    except:
        pass
