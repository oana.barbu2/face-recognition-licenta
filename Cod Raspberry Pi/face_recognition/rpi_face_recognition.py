# Importarea pachetelor sau bibliotecilor necesare
from imutils.video import VideoStream  # Pentru prelucrarea fluxului (stream) video
import face_recognition  # Pentru functionalitatea de recunoastere faciala
import imutils  # Pentru procesarea de imagini
import pickle  # Pentru serializarea si deserializarea obiectelor Python
import time  # Functii legate de timp
import cv2  # Biblioteca OpenCV pentru sarcini de computer vision
import requests  # Pentru realizarea de cereri HTTP
import urllib.parse  # Pentru codificare si decodare URL

# Initializarea 'currentname' sa se declanseze doar cand o persoana noua este identificata
name_current = "unknown"

# Calea fisierului pentru modelul antrenat pentru recunoastere faciala
path_encodings = "encodings.pickle"

# Incarcarea fetelor si codificarilor cunoscute
print("Incarcarea codificarilor si a detectorului de fete...")
data = pickle.loads(open(path_encodings, "rb").read())

# Initializarea fluxului video
video_stream = VideoStream(usePiCamera=True).start()
time.sleep(2.0)

# Functia care face un apel API si trimite numele persoanei
def make_api_call(name):
    base_url = "https://api-v2.voicemonkey.io/announcement"
    token = "d78648e9281e6df57135938d15a4f3d1_bef2e84a90452651cc479d07128cbc4f"
    device = "echo-dot"
    text = f"This person is {name}"

    # Codarea parametrului de tip text
    encoded_text = urllib.parse.quote(text)

    # Construirea adresei URL completa cu parametri
    url = f"{base_url}?token={token}&device={device}&text={encoded_text}"

    try:
        response = requests.get(url)  # Realizarea cererii GET (pentru a prelua date)
        response.raise_for_status()  # Ridicarea unei exceptii daca cererea a esuat
        data = response.json()  # Extragerea datelor JSON din raspuns

        # Printarea datelor
        print(data)

    except requests.exceptions.RequestException as e:
        # Printarea erorilor
        print("Error: ", e)
        
def display_boxes_names():
    # Desenarea numelui prezis pe imagine
    cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 225), 2)
    y = top - 15 if top - 15 > 15 else top + 15
    cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_COMPLEX, .8, (0, 255, 255), 2)

# Trecerea in bucla prin cadrele din fluxul video
while True:
    # Preluarea cadrului si redimensionarea acestuia la 500px pt a accelera procesarea
    frame = video_stream.read()
    frame = imutils.resize(frame, width=500)

    # Detectarea casetelor pentru fata
    bounding_boxes = face_recognition.face_locations(frame)

    # Realizarea unui embedding pentru fiecare caseta de delimitare a fetei
    encodings = face_recognition.face_encodings(frame, bounding_boxes)
    names = []

    # Trecerea in bucla prin embeddings faciale
    for encoding in encodings:
        # Incercarea de a potrivi fiecare fata din imaginea primita cu codificarile cunoscute
        matches = face_recognition.compare_faces(data["encodings"], encoding)
        name = "Unknown"  # Daca fata nu este recunoscuta, seteaza numele ca Unknown (necunoscut)

        # Verificarea daca s-a gasit o potrivire
        if True in matches:
            # Gasirea indecsilor tuturor fetelor potrivite si initializarea unui dictionar care sa numere numarul total de ori pentru care fiecare fata a fost potrivita
            matchedIdxs = [i for (i, b) in enumerate(matches) if b]
            total = {}

            # Trecerea in bucla printre indecsii potriviti si mentinerea numaratorii pentru fiecare fata recunoscuta
            for i in matchedIdxs:
                name = data["names"][i]
                total[name] = total.get(name, 0) + 1

            # Determinarea fetei recunoscute cu cel mai mare numar de voturi
            # Nota: In cazul unei egalitati de voturi, Python va selecta prima intrare din dictionar
            name = max(total, key=total.get)

            # Daca cineva din dataset este identificat, se printeaza numele persoanei si se face un apel API ca Alexa sa anunte persoana
            if name_current != name:
                name_current = name
                print(name_current)
                make_api_call(name_current)

        # Actualizarea listei cu nume
        names.append(name)

    # Trecerea in bucla prin fetele recunoscute
    for ((top, right, bottom, left), name) in zip(bounding_boxes, names):
        display_boxes_names()

    # Afisarea imaginii pe monitor
    cv2.imshow("Recunoasterea Faciala functioneaza...", frame)
    key = cv2.waitKey(1) & 0xFF

    # Iesirea din bucla daca se apasa tasta 'q'
    if key == ord("q"):
        break

print("Recunoastere faciala incheiata")

# Oprirea fluxului video si inchiderea ferestrelor
cv2.destroyAllWindows()
video_stream.stop()
