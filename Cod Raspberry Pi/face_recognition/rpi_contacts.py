# Importarea bibliotecilor necesare

import cv2 # Biblioteca OpenCV pentru sarcini de computer vision
from picamera import PiCamera # Biblioteca PiCamera pentru interfata camerei Raspberry Pi
from picamera.array import PiRGBArray # Modulul PiRGBArray pentru captarea imaginilor din PiCamera
import os # Interfata sistemului de operare
import requests # Pentru realizarea de cereri HTTP
import urllib.parse # Pentru codificare si decodare URL

# Modifica numele (inlocuieste cu numele persoanei pe care vrei sa o adaugi)
name = 'Oana'

# Functia care face un apel API si anunta utilizatorul
def make_api_call():
    base_url = "https://api-v2.voicemonkey.io/announcement"
    token = "d78648e9281e6df57135938d15a4f3d1_bef2e84a90452651cc479d07128cbc4f"
    device = "echo-dot"
    text = f"The photos were taken. The model is being trained."

    # Codarea parametrului de tip text
    encoded_text = urllib.parse.quote(text)

    # Construirea adresei URL completa cu parametri
    url = f"{base_url}?token={token}&device={device}&text={encoded_text}"

    try:
        response = requests.get(url)  # Realizarea cererii GET (pentru a prelua date)
        response.raise_for_status()  # Ridicarea unei exceptii daca cererea a esuat
        data = response.json()  # Extragerea datelor JSON din raspuns

        # Printarea datelor
        print(data)

    except requests.exceptions.RequestException as e:
        # Printarea erorilor
        print("Error: ", e)


# Crearea unui folder cu numele persoanei daca acesta nu exista
folder_path = "dataset/" + name
if not os.path.exists(folder_path):
    os.makedirs(folder_path)

# Initializarea obiectului PiCamera
camera = PiCamera()

# Setarea rezolutiei camerei - rezolutia 720p HD
camera.resolution = (1280, 720)

# Setarea ratei de cadre a camerei
camera.framerate = 10

# Initializarea PiRGBArray pentru a captura cadre
record = PiRGBArray(camera, size=(1280, 720))
    
# Contor pentru numele imaginilor
counter_images = 0

# Pornirea unei bucle infinite pentru capturarea imaginilor
while True:
    # Capturarea continua a cadrelor din camera
    for frame in camera.capture_continuous(record, format="bgr", use_video_port=True):
        # Accesarea cadrului curent
        image = frame.array
        
        # Afisarea cadrului pe monitor
        cv2.imshow("Poti apasa Space sa faci o poza SAU ESC pentru a inchide", image)
        
        # Curatarea fluxului video pentru pregatirea urmatorului cadru
        record.truncate(0)
    
        # Asteptare pentru apasarea unei taste
        key = cv2.waitKey(1)
        
        # Curatarea fluxului video din nou inainte de a captura un nou cadru
        record.truncate(0)
        
        # Verificarea tastei apasate
        if key%256 == 27:  # Daca a fost apasata tasta ESC
            break #Se iese din bucla
        elif key%256 == 32:  # Daca a fost apasata tasta SPACE
            # Crearea numelui de fisier pentru imaginea capturata
            name_image = "dataset/" + name + "/image_{}.jpg".format(counter_images)
            
            # Salvarea imaginii in locatia specificata
            cv2.imwrite(name_image, image)
            
            # Printarea mesajului care indica faptul ca poza a fost salvata
            print("{} salvata!".format(name_image))
            
            # Incrementarea contorului de imagini
            counter_images += 1
            
    # Verificarea daca tasta ESC a fost apasata pentru a iesi din bucla
    if key%256 == 27:
        print("Scriptul se inchide...")
        make_api_call() # Realizarea apelului API pentru a anunta utilizatorul ca urmeaza antrenarea modelului
        cv2.destroyAllWindows() # Inchiderea tuturor ferestrelor
        os.system("python /home/licenta1/face_recognition/rpi_train_contacts.py")
        break


