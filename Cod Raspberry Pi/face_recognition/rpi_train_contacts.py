# Importarea bibliotecilor si pachetelor necesare
from imutils import paths  # Pachet care lucreaza cu cai de fisiere
import face_recognition  # Pachet pentru sarcinile de recunoastere faciala
import pickle  # Pentru serializarea si deserializarea obiectelor Python
import cv2  # Biblioteca OpenCV pentru sarcini de computer vision
import os  # Interfața cu sistemul de operare
import requests # Pentru realizarea de cereri HTTP
import urllib.parse # Pentru codificare si decodare URL

# Informarea ca datele necesare sunt importate
print("Importarea datelor...")

# Obtinerea cailor imaginilor din folderul "dataset"
paths_images = list(paths.list_images("dataset"))

# Initializarea listelor de codificari cunoscute si nume cunoscute
encodings_known = []
names_known = []

# Functia care face un apel API si anunta utilizatorul
def make_api_call():
    base_url = "https://api-v2.voicemonkey.io/announcement"
    token = "d78648e9281e6df57135938d15a4f3d1_bef2e84a90452651cc479d07128cbc4f"
    device = "echo-dot"
    text = f"The person was added to contacts"

    # Codarea parametrului de tip text
    encoded_text = urllib.parse.quote(text)

    # Construirea adresei URL completa cu parametri
    url = f"{base_url}?token={token}&device={device}&text={encoded_text}"

    try:
        response = requests.get(url)  # Realizarea cererii GET (pentru a prelua date)
        response.raise_for_status()  # Ridicarea unei exceptii daca cererea a esuat
        data = response.json()  # Extragerea datelor JSON din raspuns

        # Printarea datelor
        print(data)

    except requests.exceptions.RequestException as e:
        # Printarea erorilor
        print("Error: ", e)

# Functia pentru antrenarea modelului        
def encoding_faces(model: str = "hog"):
    # Detectarea coordonatelor casetelor de delimitare corespunzatoare fiecarei fete din imaginea incarcata
    bounding_boxes = face_recognition.face_locations(rgb, model=model)

    # Realizarea de codari pentru fete
    encodings = face_recognition.face_encodings(rgb, bounding_boxes)

    # Trecerea in bucla prin codari
    for encoding in encodings:
        # Adaugarea fiecarei codari si fiecarui nume pe listele de codari si nume cunoscute
        encodings_known.append(encoding)
        names_known.append(name)

# Trecerea in bucla prin caile imaginilor
for (i, path_image) in enumerate(paths_images):
    # Extragerea numelui persoanei din calea imaginii
    print("Procesarea imaginii {}/{}".format(i + 1, len(paths_images)))
    name = path_image.split(os.path.sep)[-2]

    # Incarcarea imaginii si convertirea din ordonarea OpenCV in ordonarea dlib
    image = cv2.imread(path_image)
    rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    
    encoding_faces()

# Serializarea codarilor si numelor cunoscute
print("Serializarea codificarilor...")
data = {"encodings": encodings_known, "names": names_known}
f = open("encodings.pickle", "wb")
f.write(pickle.dumps(data))
f.close()

# Realizarea unui apel API care anunta utilizatorul ca persoana a fost adaugata la contacte
make_api_call()
