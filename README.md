Sistem de recunoaștere facială cu comandă vocală

Configurare Raspberry Pi

- Instalarea Raspberry Pi OS pe un card SD
- Folderul „face_recognition” din „Cod Raspberry Pi” să se afle pe cardul SD
- În folderul dataset, vor fi salvate pozele persoanelolr în foldere cu numele fiecăreia
- alexa_to_rpi.py - script verificare mesaje din SQS
- rpi_face_recognition.py - script recunoaștere facială
- rpi_contacts.py - script adăugare persoane
- rpi_train_contacts.py - script antrenare model

După antrenarea modelului, va apărea fisierul encodings.pickle care conține codificările
persoanelor adăugate.

Pentru a putea rula codul este necesară instalarea unor pachete care sunt specificate în
documentație la capitolul 5.1. Programarea plăcii Raspberry Pi


Configurare funcția Lambda

- Anterior, trebuie urmați pașii de creare al skill-ului pentru Alexa și al serviciilor Amazon
specificate în capitolele 5.4-5.8 din documentație
- Codul din folderul „Cod Functie Lambda” este logica din spatele skill-ului Alexa și sunt necesare
setările de mai sus și scrierea acestuia în funcția Lambda creată pentru a funcționa corespunzător
